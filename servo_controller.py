import RPi.GPIO as GPIO
#import MySQLdb
from time import sleep

#=== Init var for Pinout ===
servo_control1=14
servo_control2=15
servo_control3=18
servo_control4=23

GPIO.setmode(GPIO.BCM)

#=== Set pin Mode as Output ===
GPIO.setup(servo_control1,GPIO.OUT)
GPIO.setup(servo_control2,GPIO.OUT)
GPIO.setup(servo_control3,GPIO.OUT)
GPIO.setup(servo_control4,GPIO.OUT)

#=== PWM Init ===
pwm1=GPIO.PWM(servo_control1, 50)
pwm2=GPIO.PWM(servo_control2, 50)
pwm3=GPIO.PWM(servo_control3, 50)
pwm4=GPIO.PWM(servo_control4, 50)

#=== PWM Start ===
pwm1.start(0)
pwm2.start(0)
pwm3.start(0)
pwm4.start(0)

#=== set specific angle of motor ===
def SetAngle1(angle):
	duty = angle / 18 + 2
	GPIO.output(servo_control1, True)
	pwm1.ChangeDutyCycle(duty)

def SetAngle2(angle):
	duty = angle / 18 + 2
	GPIO.output(servo_control2, True)
	pwm2.ChangeDutyCycle(duty)

def SetAngle3(angle):
	duty = angle / 18 + 2
	GPIO.output(servo_control3, True)
	pwm3.ChangeDutyCycle(duty)

def SetAngle4(angle):
	duty = angle / 18 + 2
	GPIO.output(servo_control4, True)
	pwm4.ChangeDutyCycle(duty)
	
while True:
        #degree max is 180
        SetAngle1(0)
        SetAngle2(0)
        SetAngle3(0)
        SetAngle4(0)
        sleep(1)
        SetAngle1(160)
        SetAngle2(180)
        SetAngle3(160)
        SetAngle4(180)
        sleep(1)
